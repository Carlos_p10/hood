import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class ProfileEditPage extends StatefulWidget {
  @override
  _ProfileEditPageState createState() => _ProfileEditPageState();
}

class _ProfileEditPageState extends State<ProfileEditPage> {

  String _nombre = '';
  String _email = '';
  String _fecha = '';

  TextEditingController _inputDateController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Perfil",
          style: GoogleFonts.muli(
              color: Colors.black,
              fontSize: 19,
              fontWeight: FontWeight.w600,
          ),
        ),
        backgroundColor: Color(0xFFFAFAFA),
        elevation: 3,
        centerTitle: true,
      
      ),
      body: ListView(  
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),      
        children: <Widget>[ 
          SizedBox(height: 35.0),
          _pictureRounded(),
          SizedBox(height: 35.0),
          Divider(),                   
          _crearInput(),
          Divider(),
          _crearEmail(),
          Divider(),
          _crearFecha(context),
          Divider(),
          _logout(),
          Divider(),
        ],
      ),
    );
  }

  Widget _pictureRounded() {
    return Container(
      width: 100.0,
      height: 100.0,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20.0),
        child: Image.network('https://dam.menshealthlatam.com/wp-content/uploads/2018/11/STANLEE.jpg',
          fit: BoxFit.contain,),
      ),
    );

  }

  Widget _crearInput() {
    return TextField(
      textCapitalization: TextCapitalization.sentences, 
      decoration: InputDecoration(
        
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        hintText: 'Nombre',
        labelText: 'Nombre',        
        suffixIcon: Icon(Icons.accessibility),
        icon: Icon(Icons.account_circle)
      ),
      onChanged: (valor){        
        setState(() {
          _nombre = valor; 
        });
      },
    ); 

  }

  Widget _crearEmail() {

    return TextField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        hintText: 'Email',
        labelText: 'Email',
        suffixIcon: Icon(Icons.alternate_email),
        icon: Icon(Icons.email)
      ),
      onChanged: (valor) => setState(() {
          _email = valor; 
        })
    ); 

  }

  Widget _crearFecha(BuildContext context) {
    return TextField(
      enableInteractiveSelection: false,
      controller: _inputDateController,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        hintText: 'Fecha de Nacimiento',
        labelText: 'Fecha de Nacimiento',
        suffixIcon: Icon(Icons.perm_contact_calendar),
        icon: Icon(Icons.calendar_today)
      ),
      onTap: (){

        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(context);

      },
    ); 
  }

  _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context, 
      initialDate: new DateTime.now(), 
      firstDate: new DateTime(2019), 
      lastDate: new DateTime(2025),
      locale: Locale('es')
      );
      

      if (picked != null) {
        setState(() {
          _fecha = picked.toString();
          _inputDateController.text = _fecha; 
        });
      }
  }

  Widget _logout() {
    return GestureDetector(
      
      onTap: (){
        setState(() {
          
        });
        Navigator.pushReplacementNamed(context, 'login');
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 15, horizontal: 5),
        child: Row(        
          children: <Widget>[
            Icon(FontAwesomeIcons.signOutAlt, color: Colors.grey,),
            SizedBox(width: 25),
            Text('Cerrar Sesion', style: GoogleFonts.muli(
              fontSize: 18,
              color: Color.fromRGBO(128, 128, 128, 2)
            ),)
          ],
        ),
      ),
    );
  }
}