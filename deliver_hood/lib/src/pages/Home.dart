import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:deliver_hugo_copy/src/pages/OrderDescription.dart';


class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}
class _HomeState extends State<Home> {  
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFFAFAFA),
        elevation: 3,
        title: Center(
          child: Text(
            "Hood",
            style: GoogleFonts.muli(
              color: Colors.black,
              fontSize: 19,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        brightness: Brightness.light,        
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          _crearTipo1(),
          SizedBox(height: 20.0),
          _crearTipo1(),
          SizedBox(height: 20.0),
          _crearTipo1(),
          SizedBox(height: 20.0),
          _crearTipo1(),
          SizedBox(height: 20.0),
          _crearTipo1(),
          SizedBox(height: 20.0),
          _crearTipo1(),
          SizedBox(height: 20.0),
          //_cardTipo2(),
        ],
      ),
    );
  }

  Widget _crearTipo1() {
    return Card(
      elevation: 8.0, //para darle sombra a una tarjeta
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)), //para darle los bordes redondeados
      child: Column(
        children: <Widget>[
          ListTile(
            leading: leadingImg(),
            title: Text('# Orden 41',
                style: GoogleFonts.muli(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
            )),
            subtitle: Text('Paseo General Escalon, San Salvador.',
                  style: GoogleFonts.muli(
                    color: Colors.black54,
                    fontSize: 17,
                    //fontWeight: FontWeight.w100,
                  )),            
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(onPressed: () {
                setState(() {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => OrderDescriptionPage()));
                });
              },
               child: Text('Aceptar',
                style: GoogleFonts.muli(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
            ),)),
            ],
          )
        ],
      ),
    );
  }

  Widget leadingImg(){
    final image = FadeInImage(
      placeholder: AssetImage('assets/images/loading.gif'), 
      image: NetworkImage('https://d1kxxrc2vqy8oa.cloudfront.net/wp-content/uploads/2020/01/09214916/RFB-2312-2-tacos.jpg'),
      fadeInDuration: Duration(milliseconds: 200),
      height: 70.0,
      fit: BoxFit.cover,
    );
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.white,
        boxShadow: <BoxShadow> [
          BoxShadow(
            color: Colors.black38,
            blurRadius: 10.0, //agrega sombra
            spreadRadius: 2.0, //espace mas la sombre
            offset: Offset(2.0, 10.0) //determina la posicion de la sombra
          ),
        ]
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15.0),
        child: image,
      ),
    );
  }    
}
