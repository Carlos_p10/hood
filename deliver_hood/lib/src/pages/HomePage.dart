import 'package:deliver_hugo_copy/src/pages/HistorySalesPage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:deliver_hugo_copy/src/pages/Home.dart';
import 'package:deliver_hugo_copy/src/pages/ProfileEditPage.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}
class _HomePageState extends State<HomePage> {

  int _selectedIndex = 0;

  final pages = [Home(), HistorySalesPage(), ProfileEditPage()]; //Esta es la lista para ir cambiando entre pestanias en el bottom navBar  

  @override
  Widget build(BuildContext context) {
    return Scaffold(      
      body: pages[_selectedIndex],
      bottomNavigationBar: _bottomNavBar(),
    );
  }

  Widget _bottomNavBar(){   
    return BottomNavigationBar(          
      type: BottomNavigationBarType.fixed,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(FontAwesomeIcons.home),
          title: SizedBox(),
        ),
        BottomNavigationBarItem(
          icon: Icon(FontAwesomeIcons.chartBar),
          title: SizedBox(),
        ),
        BottomNavigationBarItem(
          icon: Icon(FontAwesomeIcons.userAlt),
          title: SizedBox(),
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Color(0xFFfd5352),
      onTap: (index){
        setState(() {
          _selectedIndex = index;
      });
      },
    );
  }

}
