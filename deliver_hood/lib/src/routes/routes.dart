
import 'package:deliver_hugo_copy/src/pages/login_page.dart';
import 'package:deliver_hugo_copy/src/pages/registro_page.dart';
import 'package:flutter/material.dart';
import 'package:deliver_hugo_copy/src/pages/HomePage.dart';


Map<String, WidgetBuilder> getApplicationRoutes(){
  return<String, WidgetBuilder>{
    '/' : (BuildContext context) => HomePage(),
    'login': (BuildContext context) => LoginPage(),
    'registro': (BuildContext context) => RegistroPage(),

  };
}