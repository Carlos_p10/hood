import 'package:flutter/material.dart';
import 'package:user_hood/src/bloc/provider.dart';
import 'package:user_hood/src/preferences_user/preferences_user.dart';
import 'package:user_hood/src/providers/usuario_provider.dart';
import 'package:user_hood/src/utils/utils.dart';

class LoginPage extends StatelessWidget {
  final prefs = new PreferenciasUsuario();
  final usuarioProvider = new UsuarioProvider();
  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _setImageCover(bloc, context),
          ],
        ),
      ),
    );
  }

  Widget _setImageCover(LoginBloc bloc, BuildContext context){
    final height = MediaQuery.of(context).size; 
    
    final container = Container(
      height: height.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage('assets/background.jpg')
        )
      ),
    );

    return Stack(
      children: <Widget>[
        container,
        Container(
          padding: EdgeInsets.only(top:80),
          child: Column(
            children: <Widget>[
              SizedBox(height: height.height*0.10, width: double.infinity,),
              Text('Hood',
              style: TextStyle(
                fontWeight: FontWeight.w900,
                color: Colors.white, 
                fontSize: 50
              )),
              loginCard(context,bloc)
            ],
          ),
        )
      ],
    );
  }

  Widget loginCard(BuildContext context, LoginBloc bloc) {
    
    return Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15),
            topRight: Radius.circular(15)
          )
        ),
        padding: EdgeInsets.symmetric(
          vertical: 40,
          horizontal: 40
        ),
        margin: EdgeInsets.symmetric(
          vertical: 80,
        ),
        child:Row(
        children: <Widget>[
          Expanded(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Iniciar sesión',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w900
                  ),
                ),
                Text('Ingresa tus datos'),
                SizedBox(height: 20),
                _setUserInput(bloc),
                SizedBox(height: 20),
                _setPasswordInput(bloc),
                SizedBox(height: 20),
                _setButtonLogin(bloc),
                _optionsUser(context)
              ],
            ),
          ),
        ]
      )
    );
  }

  Widget _setUserInput(LoginBloc bloc){

    return StreamBuilder(
      stream: bloc.emailStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return TextField(
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            hintText: 'Usuario',
            prefixIcon: Icon(Icons.account_circle),
            filled: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              borderSide: BorderSide.none,
            ),
            fillColor: Colors.black12,
            errorText: snapshot.error,
          ),
          onChanged: bloc.changeEmail,
        );
      },
    );
  }

  Widget _setPasswordInput(LoginBloc bloc){
    return StreamBuilder(
      stream: bloc.passwordStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return TextField(
          obscureText: true,
          decoration: InputDecoration(
            hintText: 'Contraseña',
            prefixIcon: Icon(Icons.vpn_key),
            filled: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              borderSide: BorderSide.none,
            ),
            fillColor: Colors.black12
          ),
          onChanged: bloc.changePassword,
        );
      },
    );
  }

  Widget _setButtonLogin(LoginBloc bloc){
    return StreamBuilder(
      stream: bloc.formValidStream2,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return RaisedButton(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 20),
            width: double.infinity,
            child: Center(
              child: Text('Ingresar'),
            ),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0)
          ),
          elevation: 0.0,
          color: Colors.deepOrange,
          textColor: Colors.white,
          onPressed: snapshot.hasData ? ()=> _login(bloc, context) : null
        );
      }
    );
  }

  Widget _optionsUser(BuildContext context) { 

    return Container(
      child: Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 50),
            /*GestureDetector(
              onTap: (){},
              child: Text('He olvidado mi contraseña', style: TextStyle(
                color: Colors.deepOrange
              ),),
            ),
            SizedBox(height: 50),*/
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 30),
                Text('No tienes cuenta todavia?'),
                SizedBox(width: 5),
                GestureDetector(
                  onTap: ()=> Navigator.pushReplacementNamed(context, 'registro'),
                  child: Text('Registrarme', style: TextStyle(
                    color: Colors.deepOrange
                  )),
                )
              ],
            )
          ],
        ),
      )
    );
  }

  _login(LoginBloc bloc, BuildContext context) async {
    Map info = await usuarioProvider.login(bloc.email, bloc.password);
    if (info['ok']) {
      prefs.loggedIn = true;
      Navigator.pushReplacementNamed(context, '/');      
    } else {
      mostrarAlerta(context, info['mensaje']);
    }
    
  }
}