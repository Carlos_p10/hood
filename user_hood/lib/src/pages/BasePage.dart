import 'package:flutter/material.dart';
import 'package:user_hood/src/pages/HomePage.dart';
import 'package:user_hood/src/pages/ProfilePage.dart';
import 'package:user_hood/src/pages/Restaurants.dart';
import 'package:user_hood/src/pages/cartPager.dart';
import 'package:user_hood/src/utils/utils.dart';
class BasePage extends StatefulWidget {
  @override
  _BasePageState createState() => _BasePageState();
}

class _BasePageState extends State<BasePage> {

  int _actualPage = 0;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _setPage(_actualPage),
      bottomNavigationBar: _setBottomNavigationBar(),
    );
  }

  Widget _setBottomNavigationBar(){
    return new Theme(data: Theme.of(context).copyWith(
        canvasColor: '#CA4C17'.toColor(),
        primaryColor: Colors.pinkAccent,
        textTheme: Theme.of(context).textTheme.copyWith(
          caption: TextStyle( color: Colors.white )
        )
      ),
      child:  BottomNavigationBar(
        onTap: (index)=>_changePage(index),
        currentIndex: _actualPage,
        selectedItemColor: '#FFAA7A'.toColor(),
        selectedIconTheme: IconThemeData(color: '#FFAA7A'.toColor()),
        unselectedIconTheme: IconThemeData(color: Colors.white),
        unselectedItemColor: Colors.grey,
        showSelectedLabels: true,

        items: [
          _setBottomNavigationItem(
            icon: Icons.fastfood,
            text: 'Inicio'
          ),
         
          _setBottomNavigationItem(
            icon: Icons.favorite,
            text: 'Favoritos'
          ),
          _setBottomNavigationItem(
            icon: Icons.shopping_cart,
            text: 'Categorias'
          ),
          _setBottomNavigationItem(
            icon: Icons.account_circle,
            text: 'Mi perfil'
          )
        ],
      ),
    );
  }

  BottomNavigationBarItem _setBottomNavigationItem({IconData icon, text}){
    return BottomNavigationBarItem(
        icon: Icon(icon),
        title:Text(text)
    );
  }

  Widget _setPage(int index){
    switch(index){
      case 0:
      return HomePage();
      case 2:
      return CartPage();
      case 3:
      return ProfilePage();
      default:
      return HomePage();

    }
  }

  _changePage(int index) => setState(() => _actualPage = index);
}