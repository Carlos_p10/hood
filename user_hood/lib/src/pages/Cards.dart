import 'package:flutter/material.dart';
import 'package:user_hood/src/utils/utils.dart';
class CardsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(
          vertical:20,
          horizontal:20
        ),
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment:CrossAxisAlignment.start,
              children: <Widget>[
                Text('Mis Tarjetas', style:TextStyle(
                  fontSize: 30, 
                  fontWeight: FontWeight.w700
                )),
                SizedBox(height: 20),
                Container(
                  decoration: BoxDecoration(
                    color: '#CA4C17'.toColor(),
                    borderRadius: BorderRadius.circular(10)
                  ),
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(
                    horizontal:20,
                    vertical: 30
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Mastercard', style: TextStyle(
                        color: Colors.white
                      )),
                      SizedBox(height: 30),
                      Text('3333 3345 4231 2343', style: TextStyle(
                        fontSize: 25,
                        color: Colors.white
                      )),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('Card holder', style: TextStyle(
                            color: Colors.white
                          )),
                          Text('Ex Date', style: TextStyle(
                            color: Colors.white
                          )),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('Carlos Paredes', style: TextStyle(
                            color: Colors.white
                          )),
                          Text('09/22', style: TextStyle(
                            color: Colors.white
                          ))
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(
                    horizontal:20,
                    vertical: 30
                  ),
                  decoration: BoxDecoration(
                    color: '#CA4C17'.toColor(),
                    borderRadius: BorderRadius.circular(10)
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Mastercard', style: TextStyle(
                        color: Colors.white
                      )),
                      SizedBox(height: 30),
                      Text('3333 3345 4231 2343', style: TextStyle(
                        fontSize: 25,
                        color: Colors.white
                      )),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('Card holder', style: TextStyle(
                            color: Colors.white
                          )),
                          Text('Ex Date', style: TextStyle(
                            color: Colors.white
                          )),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('Alejandro Maite', style: TextStyle(
                            color: Colors.white
                          )),
                          Text('09/22', style: TextStyle(
                            color: Colors.white
                          ))
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}