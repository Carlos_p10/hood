import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:user_hood/src/utils/utils.dart';

class DetalleComida extends StatelessWidget {
  final _style = TextStyle(color: Colors.white);
  final _style2 = GoogleFonts.inter(
    color: Colors.white,
    fontSize: 18,
    fontWeight: FontWeight.w400,
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: '#FFAA7A'.toColor(),
      body: SafeArea(
        child: SingleChildScrollView(
            child: Column(
          children: <Widget>[
            _product(context),
            _submenuRadio(),
            _submenu(),
            _buttonCart(context),
          ],
        )),
      ),
    );
  }

  Widget _product(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Column(
        children: <Widget>[
          GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Container(
              alignment: Alignment.topLeft,
              child:
                  Icon(Icons.arrow_back, size: 35, color: '#CA4C17'.toColor()),
            ),
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(5),
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(10)),
                  width: 120,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: FadeInImage(
                      fit: BoxFit.cover,
                      placeholder: AssetImage('assets/loading.gif'),
                      image: NetworkImage(
                          'https://revistamqe.com/wp-content/uploads/2016/05/MontajeTacoSupreme.png'),
                    ),
                  )),
              Container(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text('Taco al Pastor',
                        style: TextStyle(
                            fontWeight: FontWeight.w800,
                            color: Colors.white,
                            fontSize: 20)),
                    SizedBox(height: 5),
                    Text(
                      '\$ 4.50',
                      style: _style,
                    ),
                    SizedBox(height: 5),
                  ],
                ),
              )
            ],
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Text(
              'Ea voluptate enim tempor non. Eu commodo anim velit voluptate veniam consectetur magna consectetur. ',
              style: _style,
              textAlign: TextAlign.start,
            ),
          )
        ],
      ),
    );
  }

  Widget _submenu() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.topLeft,
            child: Text(
              'Menu 2',
              style: TextStyle(
                  color: '#CA4C17'.toColor(),
                  fontSize: 20,
                  fontWeight: FontWeight.w800),
            ),
          ),
          _optionCheck(true),
          _optionCheck(false),
          _optionCheck(false),
        ],
      ),
    );
  }

  Widget _optionCheck(bool value) {
    return Container(
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        // color: Colors.white,
        border: Border.all(color: Color(0xffffffff)),
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Checkbox(
              value: value, onChanged: (f) {}, activeColor: Colors.white30),
          Text('Descripcion', style: _style),
          Text('\$ 4.50', style: _style)
        ],
      ),
    );
  }

  Widget _submenuRadio() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.topLeft,
            child: Text(
              'Menu 1',
              style: TextStyle(
                  color: '#CA4C17'.toColor(),
                  fontSize: 20,
                  fontWeight: FontWeight.w800),
            ),
          ),
          _optionRadio(true),
          _optionRadio(false),
          _optionRadio(false),
        ],
      ),
    );
  }

  Widget _optionRadio(bool value) {
    return Container(
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        // color: Colors.white,
        border: Border.all(color: Color(0xffffffff)),
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Radio(
            value: value,
            groupValue: true,
            onChanged: (f) {},
            activeColor: Colors.white,
          ),
          Text('Descripcion', style: _style),
          Text('\$ 4.50', style: _style)
        ],
      ),
    );
  }

  Widget _buttonCart(BuildContext context){
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, 'cart'),
      child: Container(
        height: 60,
        margin: EdgeInsets.symmetric(horizontal: 60, vertical: 10),
        padding: EdgeInsets.all(5),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Color(0xffCA4C17),
            border: Border.all(color: Color(0xffCA4C17)),
            borderRadius: BorderRadius.circular(5.0)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Text(
                "Agregar a la orden",
                style: _style2,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(width: 10),
            Icon(FontAwesomeIcons.arrowRight, color: Colors.white, size: 20),
          ],
        ),
      ),
    );
  }
}
