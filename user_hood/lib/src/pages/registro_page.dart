import 'package:flutter/material.dart';
import 'package:user_hood/src/bloc/provider.dart';
import 'package:user_hood/src/providers/usuario_provider.dart';
import 'package:user_hood/src/utils/utils.dart';

class RegistroPage extends StatelessWidget {
  final usuarioProvider = new UsuarioProvider();
  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _setImageCover(bloc, context),
          ],
        ),
      ),
    );
  }

  Widget _setImageCover(LoginBloc bloc, BuildContext context){
    final height = MediaQuery.of(context).size; 
    
    final container = Container(
      height: height.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
        
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage('assets/background.jpg')
        )
      ),
    );

    return Stack(
      children: <Widget>[
        container,
        Container(
          padding: EdgeInsets.only(top:80),
          child: Column(
            children: <Widget>[
              SizedBox(height: 125, width: double.infinity,),
              Text('Hood',
              style: TextStyle(
                fontWeight: FontWeight.w900,
                color: Colors.white, 
                fontSize: 50
              )),
              loginCard(context, bloc)
            ],
          ),
        )
      ],
    );
  }

  Widget loginCard(BuildContext context, LoginBloc bloc) {
    
    return Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15),
            topRight: Radius.circular(15)
          )
        ),
        padding: EdgeInsets.symmetric(
          vertical: 40,
          horizontal: 40
        ),
        margin: EdgeInsets.symmetric(
          vertical: 80,
        ),
        child:Row(
        children: <Widget>[
          Expanded(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Registrarme',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w900
                  ),
                ),
                SizedBox(height: 10),
                Text('Brindanos tus datos'),
                SizedBox(height: 20),
                _setNameUser(bloc),
                SizedBox(height: 20),
                _setUserNameInput(bloc),
                SizedBox(height: 20),
                _setEmailInput(bloc),
                SizedBox(height: 20),
                _setPasswordInput(bloc),
                SizedBox(height: 20),
                _setRepeatPasswordInput(bloc),
                SizedBox(height: 20),
                _setButtonLogin(bloc),
                SizedBox(height: 20),
                _optionsUser(context)
              ],
            ),
          ),
        ]
      )
    );
  }

  Widget _setNameUser(LoginBloc bloc){
    return StreamBuilder(
      stream: bloc.nombreStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return TextField(
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            hintText: 'Nombre completo',
            prefixIcon: Icon(Icons.account_circle),
            filled: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              borderSide: BorderSide.none,
            ),
            fillColor: Colors.black12,
            errorText: snapshot.error,
          ),
          onChanged: bloc.changeNombre,
        );
      },
    );
  }

  Widget _setUserNameInput(LoginBloc bloc){
    return StreamBuilder(
      stream: bloc.userNameStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return TextField(
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            hintText: 'Usuario',
            prefixIcon: Icon(Icons.account_circle),
            filled: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              borderSide: BorderSide.none,
            ),
            fillColor: Colors.black12,
            errorText: snapshot.error,
          ),
          onChanged: bloc.changeUserName,
        );
      },
    );
  }

  Widget _setEmailInput(LoginBloc bloc){
    return StreamBuilder(
      stream: bloc.emailStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return TextField(
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            hintText: 'Correo electronico',
            prefixIcon: Icon(Icons.mail),
            filled: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              borderSide: BorderSide.none,
            ),
            fillColor: Colors.black12,
            errorText: snapshot.error,
          ),
          onChanged: bloc.changeEmail,
        );
      },
    );
  }

  Widget _setPasswordInput(LoginBloc bloc){
    return StreamBuilder(
      stream: bloc.passwordStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return TextField(
          obscureText: true,
          decoration: InputDecoration(
            hintText: 'Contraseña',
            prefixIcon: Icon(Icons.vpn_key),
            filled: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              borderSide: BorderSide.none,
            ),
            fillColor: Colors.black12
          ),
          onChanged: bloc.changePassword,
        );
      },
    );
  }

  Widget _setRepeatPasswordInput(LoginBloc bloc){
    return StreamBuilder(
      stream: bloc.rpasswordStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return TextField(
          obscureText: true,
          decoration: InputDecoration(
            hintText: 'Repita su Contraseña',
            prefixIcon: Icon(Icons.vpn_key),
            filled: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              borderSide: BorderSide.none,
            ),
            fillColor: Colors.black12
          ),
          onChanged: bloc.changeRPassword,
        );
      },
    );
  }

  Widget _setButtonLogin(LoginBloc bloc){
    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return RaisedButton(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 20),
            width: double.infinity,
            child: Center(
              child: Text('Ingresar'),
            ),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0)
          ),
          elevation: 0.0,
          color: Colors.deepOrange,
          textColor: Colors.white,
          onPressed: snapshot.hasData ? () => _register(bloc, context) : null 
        );
      }
    );
  }

  Widget _optionsUser(BuildContext context) { 
    return Container(
      width: double.infinity,
      child: Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 50),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Ya tengo una cuenta'),
                SizedBox(width: 5),
                GestureDetector(
                  onTap: ()=>Navigator.pushReplacementNamed(context, 'login'),
                  child: Text('Login', style: TextStyle(
                    color: Colors.deepOrange
                  )),
                )
              ],
            )
          ],
        ),
      )
    );
  }

  _register(LoginBloc bloc, BuildContext context) async {
    Map info = await usuarioProvider.nuevoUsuario(bloc.nombre, bloc.userName,' ', bloc.email, bloc.password, bloc.rpassword, true);
    if (info['ok']) {
      Navigator.pushReplacementNamed(context, '/');      
    } else {
      mostrarAlerta(context,info['mensaje']);
    }    
  }
}