import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:user_hood/src/utils/utils.dart';

class CartPage extends StatelessWidget {
  final _style = GoogleFonts.inter(
    color: Colors.black,
    fontSize: 19,
    fontWeight: FontWeight.w400,
  );

  final _style2 = GoogleFonts.inter(
    color: Colors.white,
    fontSize: 18,
    fontWeight: FontWeight.w400,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          // padding: EdgeInsets.only(left: 5, right: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _title(context),
              SizedBox(height: 20),
              _products(),
              SizedBox(height: 20),
              _products(),
              SizedBox(height: 20),
              _products(),
              SizedBox(height: 20),
              _promotionCode(),
              SizedBox(height: 20),
              Divider(),
              SizedBox(height: 5),
              _totalDelivery(),
              SizedBox(height: 5),
              Divider(),
              SizedBox(height: 5),
              _total(),
              SizedBox(height: 5),
              Divider(),
              SizedBox(height: 20),
              _buttonCheckout(),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  Widget _title(BuildContext context){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Container(
              alignment: Alignment.topLeft,
              child:
                  Icon(Icons.arrow_back, size: 35, color: '#CA4C17'.toColor()),
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            child: Text('Your Cart', textAlign: TextAlign.start, style: _style)),

        ],
      )
      
      
      
    
    );
  }

  Widget _products() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage("assets/images/Block.png")),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.4),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 4),
                  )
                ]),
            height: 80,
            width: 100,

            // child: Image.asset("assets/images/Block.png")
          ),
          Column(
            children: <Widget>[
              Text("Nombre"),
              Row(
                children: <Widget>[
                  Text("-"),
                  Text("N"),
                  Text("+"),
                ],
              )
            ],
          ),
          Text("precio")
        ],
      ),
    );
  }

  Widget _promotionCode() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          SizedBox(
            height: 50,
            width: 150,
            child: TextField(
              textAlign: TextAlign.center,
              textCapitalization: TextCapitalization.sentences,
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    borderSide: BorderSide(color: Colors.grey)),
                hintText: 'Agregar Codigo',
                // labelText: 'Agregar Codigo',
              ),
            ),
          ),
          Container(
            height: 50,
            width: 90,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Color(0xfffcb606)),
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.4),
                    spreadRadius: 3,
                    blurRadius: 5,
                    offset: Offset(0, 6),
                  )
                ]),
            child: Text(
              "Aplicar",
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }

  Widget _totalDelivery() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      padding: EdgeInsets.only(left: 5, right: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text("Cobro por viaje"),
          Text("\$4.0"),
        ],
      ),
    );
  }

  Widget _total() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      padding: EdgeInsets.only(left: 5, right: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text("Total "),
          Text("\$4.0"),
        ],
      ),
    );
  }

  Widget _buttonCheckout() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 60,
            width: 150,
            margin: EdgeInsets.symmetric(horizontal: 60, vertical: 10),
            padding: EdgeInsets.all(5),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Color(0xfffcb606),
                border: Border.all(color: Color(0xfffcb606)),
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.4),
                    spreadRadius: 3,
                    blurRadius: 5,
                    offset: Offset(0, 6),
                  )
                ]),
            child: Text(
              "Pagar",
              style: _style2,
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}
