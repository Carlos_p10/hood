import 'package:flutter/material.dart';
import 'package:user_hood/src/utils/utils.dart';
class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _header(),
            _listRestaurant(context)
          ],
        ),
      ),
    );
  }

  Widget _header(){

    //orange background
    final container = Container(
      width: double.infinity,
      height: 250,
      color:'#FFAA7A'.toColor(),
    );
    
    //text hood and welcome 
    final textColumn = SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 20,
          horizontal: 20
        ),
        width: double.infinity,
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
            Text('HOOD', style: TextStyle(
              color: '#CA4C17'.toColor(),
              fontSize: 40,
              fontWeight: FontWeight.w700
            )),
            Text('Bienvenido', style: TextStyle(
              color: '#CA4C17'.toColor(),
              fontSize: 25
            )),
          ],
        ),
      )
    );



    //widget to get the other widgets and put the cards over the background
    return Stack(
      children: <Widget>[
        container,
        textColumn,
        _sliderRestaurants()
      ],
    );
  }

  Widget _sliderRestaurants() {
    return Container(
          margin: EdgeInsets.only(
            top: 180,
            bottom: 10
          ),
          height: 220,
          width: double.infinity,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: 10,
            itemBuilder: (BuildContext context, index){
              return Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 10
                ),
                width: 300,
                child: Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10)
                        ),
                        child: FadeInImage(
                        fit: BoxFit.cover,
                        width: double.infinity,
                        height: 100,
                        placeholder: AssetImage('assets/loading.gif'), 
                          image: NetworkImage('https://i.imgur.com/9ibjeXO.jpg'),
                          fadeInDuration: Duration(milliseconds: 200),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 10
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Stack(
                                  children: <Widget>[
                                    Container(
                                      alignment: Alignment.topLeft,
                                      child: Text('Burguer King', style: TextStyle(
                                        fontWeight: FontWeight.w900
                                      )),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        left: 170
                                      ),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Icon(Icons.timer, size: 17),
                                          SizedBox(width: 5),
                                          Text('30 - 40 min'),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: 5),
                                Row(
                                  children: <Widget>[
                                    Icon(Icons.location_on, size: 15),
                                    SizedBox(width: 5),
                                    Text('San salvador'),
                                  ],
                                ),
                                SizedBox(height: 5),
                                Row(
                                  children: <Widget>[
                                    Icon(Icons.star, size: 15, color:Colors.yellow),
                                    Icon(Icons.star, size: 15, color:Colors.yellow),
                                    Icon(Icons.star, size: 15, color:Colors.yellow),
                                    Icon(Icons.star, size: 15, color:Colors.yellow),
                                    Icon(Icons.star, size: 15, color:Colors.yellow),
                                    SizedBox(width: 10),
                                    Text('4.5', style: TextStyle(
                                      fontSize: 12
                                    ))
                                  ],
                                ),
                                SizedBox(height: 5),
                                Container(
                                  margin: EdgeInsets.only(top:5),
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 10,
                                    vertical: 2
                                  ),
                                  child: Text('Nuevo', style: TextStyle(fontSize: 13, color: Colors.green)),
                                  decoration: BoxDecoration(
                                    color: Colors.green[100],
                                    borderRadius: BorderRadius.circular(10)
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        );
  }

  Widget _listRestaurant(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color:'#FFAA7A'.toColor(),
        borderRadius: BorderRadius.circular(10)
      ),
      margin: EdgeInsets.symmetric(
        horizontal: 10
      ),
      padding: EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 10
      ),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Text('Explorar', style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w900,
                  color: '#CA4C17'.toColor()
                )),
              ),
              Container(
              child: GestureDetector(
                onTap: ()=>Navigator.pushNamed(context, 'restaurants'),
                child: Text('Ver todos', style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w700,
                    color: '#CA4C17'.toColor()
                    )
                  ), 
                ),
              ),
            ],
          ),
          _restaurantCard(),
          _restaurantCard(),
          _restaurantCard(),
           
        ],
      ),
    );
  }

  Widget _restaurantCard({name, image, location, time}){
    return Column(
      children: <Widget>[
        Container(
          height: 120,
          margin: EdgeInsets.symmetric(
            vertical: 10
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10)
          ),
          child: Row(
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10)
                  ),
                  width: 150,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: FadeInImage(
                      fit: BoxFit.cover,
                      placeholder: AssetImage('assets/loading.gif'),
                      image: NetworkImage('https://www.brandemia.org/sites/default/files/inline/images/taco_bell_local.jpg'),
                    ),
                  )
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  vertical: 20,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Taco Bell San Salvador', style: 
                      TextStyle(
                        fontWeight: FontWeight.w800
                      )
                    ),
                    SizedBox(height: 5),
                    Row(
                      children: <Widget>[
                        Icon(Icons.location_on, size: 15,),
                        SizedBox(width: 5),
                        Text('San salvador')
                      ],
                    ),
                    SizedBox(height: 5),
                    Row(
                      children: <Widget>[
                        Icon(Icons.timer, size: 15,),
                        SizedBox(width: 5),
                        Text('30 - 40 min.')
                      ],
                    ),
                    SizedBox(height: 3),
                    Row(
                      children: <Widget>[
                        Icon(Icons.star, size: 15, color:Colors.yellow),
                        Icon(Icons.star, size: 15, color:Colors.yellow),
                        Icon(Icons.star, size: 15, color:Colors.yellow),
                        Icon(Icons.star, size: 15, color:Colors.yellow),
                        Icon(Icons.star, size: 15, color:Colors.yellow),
                        SizedBox(width: 10),
                        Text('4.5', style: TextStyle(
                          fontSize: 12
                        ))
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  
}

