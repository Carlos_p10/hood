import 'package:flutter/material.dart';
import 'package:user_hood/src/utils/utils.dart';

class RestaurantPage extends StatelessWidget {
  final _style = TextStyle(color: Colors.grey[600]);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: '#ffffff'.toColor(),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _header(context),
              _combos(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _header(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        children: <Widget>[
          GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Container(
              alignment: Alignment.topLeft,
              child:
                  Icon(Icons.arrow_back, size: 35, color: '#CA4C17'.toColor()),
            ),
          ),
          SizedBox(height: 10),
          Row(
            children: <Widget>[
              Container(
                height: 130,
                child: Row(
                  children: <Widget>[
                    Container(
                        alignment: Alignment.topCenter,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10)),
                        width: 150,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: FadeInImage(
                            fit: BoxFit.cover,
                            placeholder: AssetImage('assets/loading.gif'),
                            image: NetworkImage(
                                'https://www.brandemia.org/sites/default/files/inline/images/taco_bell_local.jpg'),
                          ),
                        )),
                    Container(
                      padding: EdgeInsets.symmetric(
                        vertical: 20,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('Taco Bell San Salvador',
                              style: TextStyle(
                                fontWeight: FontWeight.w800,
                              )),
                          SizedBox(height: 5),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.location_on,
                                color: Colors.grey[600],
                                size: 15,
                              ),
                              SizedBox(width: 5),
                              Text(
                                'San salvador',
                                style: _style,
                              )
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.timer,
                                color: Colors.grey[600],
                                size: 15,
                              ),
                              SizedBox(width: 5),
                              Text(
                                '30 - 40 min.',
                                style: _style,
                              )
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _combos(BuildContext context) {
    return Container(
      width: double.infinity,
      // color:'#FFAA7A'.toColor(),
      decoration: BoxDecoration(
          color: '#FFAA7A'.toColor(), borderRadius: BorderRadius.circular(10)),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Column(
        children: <Widget>[
          _combosCard(context),
          _combosCard(context),
          _combosCard(context),
        ],
      ),
    );
  }

  Widget _combosCard(BuildContext context) {
    return GestureDetector(
        onTap: () => Navigator.pushNamed(context, 'detalleComida'),
        child: Container(
            // height: ,
            margin: EdgeInsets.symmetric(vertical: 5,horizontal: 5),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10)),
                        width: 120,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: FadeInImage(
                            fit: BoxFit.cover,
                            placeholder: AssetImage('assets/loading.gif'),
                            image: NetworkImage(
                                'https://revistamqe.com/wp-content/uploads/2016/05/MontajeTacoSupreme.png'),
                          ),
                        )),
                    Container(
                      padding: EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 40
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('Taco al Pastor',
                              style: TextStyle(fontWeight: FontWeight.w800)),
                          SizedBox(height: 5),
                          Text('\$ 4.50'),
                          SizedBox(height: 5),
                          Row(
                            children: <Widget>[
                              Icon(Icons.star, size: 15, color: Colors.yellow),
                              Icon(Icons.star, size: 15, color: Colors.yellow),
                              Icon(Icons.star, size: 15, color: Colors.yellow),
                              Icon(Icons.star, size: 15, color: Colors.yellow),
                              Icon(Icons.star, size: 15, color: Colors.yellow),
                              SizedBox(width: 10),
                              Text('5.0', style: TextStyle(fontSize: 12))
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  child: Text('Ea voluptate enim tempor non. Eu commodo anim velit voluptate veniam consectetur magna consectetur. ',
                    textAlign: TextAlign.start,
                  ),
                )
              ],
            )
            ));
  }
}
