class Menus {
    Menus({
        this.data,
        this.status,
    });

    List<Menu> data;
    bool status;

    factory Menus.fromJson(Map<String, dynamic> json) => Menus(
        data: List<Menu>.from(json["data"].map((x) => Menu.fromJson(x))),
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "status": status,
    };
}

class Menu {
    Menu({
        this.estado,
        this.positivos,
        this.negativos,
        this.id,
        this.nombre,
        this.descripcion,
        this.precio,
        this.categoria,
        this.usuario,
        this.comentarios,
        this.createdAt,
        this.updatedAt,
        this.v,
    });

    bool estado;
    List<dynamic> positivos;
    List<dynamic> negativos;
    String id;
    String nombre;
    String descripcion;
    String precio;
    String categoria;
    String usuario;
    List<Comentario> comentarios;
    DateTime createdAt;
    DateTime updatedAt;
    int v;

    factory Menu.fromJson(Map<String, dynamic> json) => Menu(
        estado: json["estado"],
        positivos: List<dynamic>.from(json["positivos"].map((x) => x)),
        negativos: List<dynamic>.from(json["negativos"].map((x) => x)),
        id: json["_id"],
        nombre: json["nombre"],
        descripcion: json["descripcion"],
        precio: json["precio"],
        categoria: json["categoria"],
        usuario: json["usuario"],
        comentarios: List<Comentario>.from(json["comentarios"].map((x) => Comentario.fromJson(x))),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
    );

    Map<String, dynamic> toJson() => {
        "estado": estado,
        "positivos": List<dynamic>.from(positivos.map((x) => x)),
        "negativos": List<dynamic>.from(negativos.map((x) => x)),
        "_id": id,
        "nombre": nombre,
        "descripcion": descripcion,
        "precio": precio,
        "categoria": categoria,
        "usuario": usuario,
        "comentarios": List<dynamic>.from(comentarios.map((x) => x.toJson())),
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
    };
}

class Comentario {
    Comentario({
        this.id,
        this.rating,
        this.comment,
        this.postedBy,
    });

    String id;
    int rating;
    String comment;
    String postedBy;

    factory Comentario.fromJson(Map<String, dynamic> json) => Comentario(
        id: json["_id"],
        rating: json["rating"],
        comment: json["comment"],
        postedBy: json["postedBy"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "rating": rating,
        "comment": comment,
        "postedBy": postedBy,
    };
}
