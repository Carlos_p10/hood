import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:user_hood/src/models/menu_models.dart';

class MenuProvider{
  Response resp;
  final dio = new Dio();
  final String _url = 'https://hood-delivery.herokuapp.com/ws/menu';

  final opts= Options(followRedirects: false, 
    validateStatus: (status){
    return status < 500;
  });
  
  Future<List<Menu>> _procesarRespuesta(String url) async { 
    final resp = await dio.get(url, options: opts);//si no funciona el String url y el dio cambiarlo por el http
    final decodedData = resp.data;
    final menus = new Menus.fromJson(decodedData['data']);
    return menus.data;
  }

  Future<List<Menu>> getMenu() async { 
    final String url = '$_url/listar';
    return await _procesarRespuesta(url);
  }

  Future<List<Menu>> getMenuxUser(String userCode) async { 
    final String url = '$_url/listar/usuario/$userCode';
    return await _procesarRespuesta(url);
    // print(uri);
    // return [];
    // return await _procesarRespuesta(url);
  }

  Future<List<Menu>> getMenuxCategory(String cateCode) async { 
    final String url = '$_url/listar/categoria/$cateCode';
    return await _procesarRespuesta(url);
  }

  Future<List<Menu>> getMenuxUserxCategory(String userCode, String cateCode) async { 
    final String url = '$_url/listar/categoria/$cateCode/usuario/$userCode';
    return await _procesarRespuesta(url);
  }

  Future<List<Menu>> getMenuDetalle(String menuId) async { 
    final String url = '$_url/$menuId';
    return await _procesarRespuesta(url);
  }

  Future<Map<String, dynamic>> postMenuComment(String userCode,int rate, String comment, String postBy) async { 
    final String url = '$_url/$userCode/comentario';
    final menuComment = {
      'rating' : rate,
      'comment' : comment,
      'postedBy' : postBy  
    };
    resp = await dio.put(
      url,
      data: json.encode(menuComment),
      options: opts
    );

    Map<String, dynamic> decodedResp = resp.data; 
    return{'ok': true, 'token': decodedResp['mensaje']};
  }

  

}