import 'dart:convert';
import 'package:user_hood/src/preferences_user/preferences_user.dart';
import 'package:dio/dio.dart';

class UsuarioProvider{
  Response resp;
  final dio = new Dio();
  final String _urlRegister = 'https://hood-delivery.herokuapp.com/ws/auth/register';
  final String _urlLogin = 'https://hood-delivery.herokuapp.com/ws/auth';
  final _prefs = new PreferenciasUsuario();

  final opts= Options(followRedirects: false, 
    validateStatus: (status){
    return status < 500;
  });

  Future<Map<String, dynamic>> login(String email, String pass)async {
    final authData = {
      'email': email,
      'pass': pass,
    };
  
    resp = await dio.post(
      _urlLogin,
      data: json.encode(authData),
      options: opts
    );

    Map<String, dynamic> decodedResp = resp.data;
    if(decodedResp['ok']){
      _prefs.token = decodedResp['data']['_id'];
      return{'ok': true, 'token': decodedResp['data']['_id']};
    } 
    else{
      return{'ok': false, 'mensaje': 'Usuario no existente'};
    }

  }

  Future<Map<String, dynamic>> nuevoUsuario(String nombre, String username, String direccion,String email, String pass, String pass2, bool isSth) async {
    final authData = {
      'nombre': nombre,
      'usuario': username,
      'direccion': direccion,
      'correo': email,
      'contrasenia': pass,
      'r_contrasenia': pass2,
      'isClient': isSth
    };

    resp = await dio.post(
      _urlRegister,
      data: json.encode(authData),
      options: opts
    );

    Map<String, dynamic> decodedResp = resp.data;
    print(decodedResp);
    print(resp.statusCode);

    if(decodedResp.containsKey('data')){
      _prefs.token = decodedResp['data']['_id'];
      return{'ok': true, 'token': decodedResp['data']['_id']};
    } else{
      return{'ok': false, 'mensaje': decodedResp['mensaje']};
    }
  }

}