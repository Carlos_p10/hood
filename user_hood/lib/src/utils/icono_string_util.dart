
import 'package:flutter/material.dart';

final _icons = <String, IconData>{
  'location_on'    : Icons.location_on,
  'payment': Icons.payment,
  'settings'  : Icons.settings,
  'help_outline'  : Icons.help_outline,
  'arrow_back'  : Icons.arrow_back,
};

Icon getIcon (String nombreIcono){
  return Icon(_icons[nombreIcono], color: Colors.red);
}

