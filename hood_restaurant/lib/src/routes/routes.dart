import 'package:flutter/material.dart';
import 'package:hood_restaurant/src/pages/HomePage.dart';
import 'package:hood_restaurant/src/pages/login_page.dart';
import 'package:hood_restaurant/src/pages/registro_page.dart';
import 'package:hood_restaurant/src/pages/OrderDetail.dart';

Map<String, WidgetBuilder> getApplicationRoutes(){
  return<String, WidgetBuilder>{
    '/' : (BuildContext context) => HomePage(),
    'login': (BuildContext context) => LoginPage(),
    'registro': (BuildContext context) => RegistroPage(),
    'detailOrder' : (BuildContext context) => OrderDetail()
  };
}