import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hood_restaurant/src/pages/OrderDescription.dart';

class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFFAFAFA),
        elevation: 3,
        title: Center(
          child: Text(
            "Hood",
            style: GoogleFonts.muli(
              color: Colors.black,
              fontSize: 19,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        brightness: Brightness.light,        
      ),
      body: ListView(
        padding: EdgeInsets.all(15),
        children: <Widget>[
          _cardFood()
        ],
      ),
    );
  }

  Widget _cardFood(){
    Color color = Color(0x33ca7f);
    
    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10)
            ),
            child: FadeInImage(
            placeholder: AssetImage('assets/images/loading.gif'), 
              image: NetworkImage('https://i.imgur.com/9ibjeXO.jpg'),
              fadeInDuration: Duration(milliseconds: 200),
            ),
          ),
          SizedBox(height: 5),
          Container(
            padding: EdgeInsets.all(15),
            child:Row(
            children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Orden No. 304', style: TextStyle(
                        fontSize: 20
                      )),
                      SizedBox(height: 5),
                      Text('Cliente: Alejandro Gonzalez', style: TextStyle(
                        color: Colors.black38,
                        fontSize: 15
                      )),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal:10, vertical: 2),
                        margin:EdgeInsets.symmetric(vertical: 10),
                        decoration: BoxDecoration(
                          color: Colors.grey[300],
                          borderRadius: BorderRadius.circular(20)
                        ),
                        child: Text('En espera', style: TextStyle(
                          color: Colors.black45
                        )),
                      ),
                      SizedBox(height: 10),
                      Container(
                        alignment: AlignmentDirectional.bottomEnd,
                        child: FlatButton(
                          color: Colors.black12,
                          onPressed: () {
                          setState(() {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => OrderDescriptionPage()));
                          });
                        },
                        child: Text('Ver orden',
                          style: GoogleFonts.muli(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                        ),)),
                      )
                    ],
                  ),
                ),
              ]
            ),
          )
        ],
      ),
    );
  }
}