import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class OrderDescriptionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _setTitle(),
              _clientDetail(context),
            ],
          ),
        ),
      ),
    );
  }

  
  Widget _setTitle(){
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Orden No. 304',
          style: TextStyle(
            fontSize: 40,
            fontWeight: FontWeight.w900
          )),
          SizedBox(height: 20),
          Text('Detalles generales',
          style: TextStyle(
            color: Colors.black38,
            fontSize: 15,
            fontWeight: FontWeight.w600
          )),
        ],
      )
    );
  }

  Widget _clientDetail(BuildContext context){
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 10
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Cliente',
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w900
          )),
          _cardUser(),
          SizedBox(height: 10),
          Text('Ubicación', style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w900
          )),
          _mapUser(),
          SizedBox(height: 10),
          Text('Detalle de pedido',style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w900
          )),
          _orderCardDetail(context),
          SizedBox(height: 10),
          Text('Metodo de pago',style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w900
          )),
          SizedBox(height: 10),
          _methodPayment(),
          SizedBox(height:40),
          Text('Total \$3.45', style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.w900
          )),
          Container(
            margin: EdgeInsets.symmetric(vertical:20),
            height: 50,
            width: double.infinity,
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10)
              ),
              onPressed: (){},
              color: Colors.orange,
              child: Text('Realizar cobro', style:TextStyle(
                color: Colors.white
              )),
            ),
          )
        ],
      )
    );
  }

  Widget _cardUser(){
    return Container(
      decoration: BoxDecoration(
        color: Colors.orange,
        borderRadius: BorderRadius.circular(10)
      ),
      margin: EdgeInsets.symmetric(
        vertical: 10
      ),
      child: Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(8) ,
            child:CircleAvatar(
              backgroundImage: NetworkImage('https://www.frontier-economics.com.au/wp-content/uploads/2018/06/daveappels_revised-500x500.jpg'),
              radius: 30,
            )
          ),
          SizedBox(width: 30),
          Text('Alejandro gonzalez', 
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w700,
              color: Colors.white,
            ) 
          )
        ],
      ),
    );
  }

  Widget _mapUser(){
    return Container(
      color: Colors.orange,
      margin: EdgeInsets.symmetric(
        vertical: 10
      ),
      width: double.infinity,
      height: 200,
      child: Center(
        child: Text('Mapa de usuario'),
      ),
    );
  }

  Widget _orderCardDetail(BuildContext context){
    return Container(
      height: 165,
      decoration: BoxDecoration(
        color: Colors.orange,
        borderRadius: BorderRadius.circular(10)
      ),
      width: double.infinity,
      padding: EdgeInsets.all(15),
      margin: EdgeInsets.symmetric(
        vertical: 10
      ),
      child: Column(
      children: <Widget>[
          Container(
            child: Row(
            children: <Widget>[
                Expanded(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Buffalo wings', style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                        )),
                        SizedBox(height: 5),
                        Text('4 items',style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w700,
                        )),
                        SizedBox(height: 15),
                        Text("Total \$3.45", style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.w700
                        )),
                      ],
                    ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(
                        bottom: 30
                      ),
                      height: 50,
                      width: 50,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child:FadeInImage(
                          fit: BoxFit.cover,
                          placeholder: AssetImage('assets/images/loading.gif'),
                          image: NetworkImage('https://dragoner.info/wp-content/uploads/2018/10/mzl.ctzgxbrx.jpg'),
                        ),
                      )
                    )
                  ],
                ),
              ],
            ),
          ),
          Expanded(
            child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)
                  ),
                  onPressed: ()=>{
                    Navigator.pushNamed(context, 'detailOrder')
                  },
                  color: Colors.white,
                  child: Text('Revisar orden'),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _methodPayment(){
    return Column(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.orange,
            borderRadius: BorderRadius.circular(10)
          ),
          padding: EdgeInsets.all(20),
          width: double.infinity,
          child: Row(
            children: <Widget>[
              Icon(
                Icons.attach_money,
                size: 30,
                color: Colors.white,
              ),
              SizedBox(width:10),
              Text('Efectivo', style: TextStyle(
                color: Colors.white
              ))
            ],
          ),
        ),
        SizedBox(height: 10),
        Container(
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border(
              top:BorderSide(color: Colors.orange),
              left:BorderSide(color: Colors.orange),
              right:BorderSide(color: Colors.orange),
              bottom:BorderSide(color: Colors.orange)
            )
          ),
          width: double.infinity,
          child: Row(
            children: <Widget>[
              Icon(
                Icons.credit_card,
                size: 30,
                color: Colors.orange,
              ),
              SizedBox(width:10),
              Text('Tarjeta', style: TextStyle(
                color: Colors.orange
              ))
            ],
          ),
        )
      ],
    );
  }
}