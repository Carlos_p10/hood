import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HistorySalesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFFAFAFA),
        elevation: 3,
        centerTitle: true,
        title: Text('Historial',
          style: GoogleFonts.muli(
            color: Colors.black,
            fontSize: 19,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}